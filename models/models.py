# -*- coding: utf-8 -*-

from odoo import models, fields, api

class missions(models.Model):
    _name = 'missions.missions'

    Objetivo = fields.Char()
    ValorPago = fields.Integer()
    Fotos = fields.Integer(string="Field Label")
    description = fields.Text()

    @api.depends('value')
    def _value_pc(self):
        self.value2 = float(self.value) / 100