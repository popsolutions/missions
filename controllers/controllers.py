# -*- coding: utf-8 -*-
from odoo import http

class Missions(http.Controller):
    @http.route('/missions/missions/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/missions/missions/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('missions.listing', {
            'root': '/missions/missions',
            'objects': http.request.env['missions.missions'].search([]),
        })

    @http.route('/missions/missions/objects/<model("missions.missions"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('missions.object', {
            'object': obj
        })